package com.example.cuncis.sqlitedemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.cuncis.sqlitedemo.model.Book;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "book.db";
    private static final String TABLE_NAME = "books";
    private static final String COL_ID = "id";
    private static final String COL_NAME = "name";
    private static final String COL_TITLE = "title";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                COL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_NAME +" TEXT, " + COL_TITLE + " TEXT);";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        onCreate(db);
    }

    public void save(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME, book.getName());
        contentValues.put(COL_TITLE, book.getTitle());

        db.insert(TABLE_NAME, null, contentValues);
        db.close();
    }

    public Book findOne(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_ID, COL_NAME, COL_TITLE},
                COL_ID + "=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );

        if (cursor != null) {
            cursor.moveToFirst();
        }

        Book book = new Book(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1),
                cursor.getString(2));

        return book;
    }

    public List<Book> findAll() {
        List<Book> listBook = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                Book book = new Book();
                book.setId(Integer.valueOf(cursor.getString(0)));
                book.setName(cursor.getString(1));
                book.setTitle(cursor.getString(2));
                listBook.add(book);
            } while (cursor.moveToNext());
        }
        return listBook;
    }

    public void update(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME, book.getName());
        contentValues.put(COL_TITLE, book.getTitle());

        db.update(TABLE_NAME, contentValues, COL_ID+"=?", new String[]{String.valueOf(book.getId())});
        db.close();
    }

    public void delete(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COL_ID+"=?", new String[]{String.valueOf(book.getId())});
        db.close();
    }
}












