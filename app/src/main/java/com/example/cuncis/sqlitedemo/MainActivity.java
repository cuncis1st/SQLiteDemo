package com.example.cuncis.sqlitedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.cuncis.sqlitedemo.database.DatabaseHelper;
import com.example.cuncis.sqlitedemo.model.Book;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper db;
    TextView tvShow;
    EditText etName, etTitle;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHelper(this);

        tvShow = findViewById(R.id.tv_show);
        etName = findViewById(R.id.et_name);
        etTitle = findViewById(R.id.et_title);
        btnSave = findViewById(R.id.btn_save);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameStr = etName.getText().toString();
                String titleStr = etTitle.getText().toString();

                String text = nameStr + "\n" + titleStr;

                db.save(new Book(nameStr, titleStr));
                tvShow.setText(text);

                etName.setText("");
                etTitle.setText("");
            }
        });




    }
}
